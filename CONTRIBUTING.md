# Contributing

The Ravn application is written in [Go](https://golang.org/). All contributions must pass through a GitLab issue and a merge request at the [Ravn Requester repository](https://gitlab.com/ravnmsg/ravn-requester).

This project is released with a Contributor Code of Conduct. Participation in this project implies agreement to abide by its terms.

## Issues

Most changes to the Ravn Requester package must go through [issues](https://gitlab.com/ravnmsg/ravn-requester/-/issues). This is where the community can discuss a specific problem or proposal.

## Merge requests

A [merge request](https://gitlab.com/ravnmsg/ravn-requester/-/merge_requests) to the `master` branch must be created. This is also where the community will discuss the code related to the current issue.

## Continuous integration

A continuous integration workflow is setup to automatically run tests on each commit. Merge requests will not be accepted if a test is failing.

## Local development

### Tests

The Ravn codebase must pass all style checks and tests. The following commands can be used locally:

```bash
# Simple command to execute tests
go test ./...

# Runs all tests
make test

# Runs style checks
make test-lint
```
