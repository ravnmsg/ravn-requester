# Ravn Requester

This is the common package for Ravn Requesters. For an actual example, the [ping requester](https://gitlab.com/ravnmsg/ravn-requester-ping) is provided.

## Description

The Deliverer service is in charge of orchestrating the delivery of a message. The last step in a delivery is to contact the third-party service that will perform the actual delivery. A Requester service is the implementation of one of that last step.

As long as a Requester service follows the standard defined below, nothing in this repository is strictly required. It nevertheless offers some helpers to remove a lot of boilerplate:

* an application helper that gracefully terminates
* the interface for requesters and input/output structs
* the interface for servers usable by the application
* an HTTP server following the required standard

Using this package, the creation of a new Requester service only requires some basic `main()` code and the step implementation.

## Standard

TODO

## Usage

This package cannot be used directly, as it only contains the tools to help build Requester services. Each implementations need to have their own repositories.

More information can be found in the various sub-packages.
