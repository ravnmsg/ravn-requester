// Package app offers a way to quickly start serving a specific requester. It is completely
// optional, but prevents a lot of boilerplate, such as the graceful termination of the application.
package app

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"

	"gitlab.com/ravnmsg/ravn-requester/pkg/requester"
	"gitlab.com/ravnmsg/ravn-requester/pkg/server"
)

// Start starts the specified srv server and registers the specified requester fn.
//
// It is expected to block until one of the following happens:
// * the provided application context terminates
// * the application receives a SIGINT
// * the server stops for any reasons
// * the server context terminates
func Start(ctx context.Context, srv server.Server, fn requester.Fn) error {
	if srv == nil {
		return fmt.Errorf("error starting server: server is nil")
	}

	if fn == nil {
		return fmt.Errorf("error starting server: requester function is nil")
	}

	serverCtx, cancel := context.WithCancel(ctx)
	defer cancel()

	// Handles graceful termination of the application.
	go func() {
		defer cancel()

		ch := make(chan os.Signal, 1)
		signal.Notify(ch, os.Interrupt)

		// Blocks until the application context ends, or an interrupt signal is caught.
		select {
		case <-ctx.Done():
		case <-ch:
		}
	}()

	log.Printf("Listening for requester requests...")

	// This is expected to block until the server context ends.
	if err := srv.ServeRequester(serverCtx, fn); err != nil {
		return fmt.Errorf("error starting server: %s", err)
	}

	log.Printf("Stopped listening for requester requests.")

	return nil
}
