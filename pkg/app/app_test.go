package app_test

import (
	"context"
	"errors"
	"fmt"
	"sync"
	"testing"
	"time"

	. "gitlab.com/ravnmsg/ravn-requester/pkg/app"
	"gitlab.com/ravnmsg/ravn-requester/pkg/requester"
	"gitlab.com/ravnmsg/ravn-requester/pkg/server"
)

func TestStartSuccess(t *testing.T) {
	srv := new(testServer)

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	go func(t *testing.T) {
		reqFn := func(_ *requester.Values) (*requester.Values, error) {
			return &requester.Values{"working": true}, nil
		}

		if err := Start(ctx, srv, reqFn); err != nil {
			t.Errorf("expected not to fail, but did with error `%s`", err)
		}
	}(t)

	// Waits a little bit for the server to start.
	time.Sleep(4 * time.Millisecond)

	if !srv.getRunning() {
		t.Fatalf("expected server to be running")
	}

	output, err := srv.getReqFn()(&requester.Values{})
	if err != nil {
		t.Fatalf("unexpected failure during step execution: %s", err)
	}

	if output.Get("working") != true {
		t.Fatalf("expected request to return working=true")
	}

	// Cancels the application context and waits a little bit for the context to terminate.
	cancel()
	time.Sleep(4 * time.Millisecond)

	if srv.getRunning() {
		t.Fatalf("expected server not to be running")
	}
}

func TestStartFailures(t *testing.T) {
	tests := map[string]struct {
		srv                  server.Server
		reqFn                requester.Fn
		expectedError        error
		expectedErrorMessage string
	}{
		"noServers": {
			srv:                  nil,
			reqFn:                func(_ *requester.Values) (*requester.Values, error) { return nil, nil },
			expectedErrorMessage: "error starting server: server is nil",
		},
		"noRequesterFn": {
			srv:                  new(testServer),
			reqFn:                nil,
			expectedErrorMessage: "error starting server: requester function is nil",
		},
		"errorStartingServer": {
			srv:                  new(unstartableServer),
			reqFn:                func(_ *requester.Values) (*requester.Values, error) { return nil, nil },
			expectedErrorMessage: "error starting server: unstartable server",
		},
	}

	for name, test := range tests {
		test := test

		t.Run(name, func(t *testing.T) {
			done := make(chan struct{})

			ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
			defer cancel()

			go func(t *testing.T) {
				if err := Start(ctx, test.srv, test.reqFn); err != nil {
					switch {
					case test.expectedError != nil && !errors.Is(err, test.expectedError):
						t.Errorf("expected error to be `%T`, but was `%T`", test.expectedError, err)
					case err.Error() != test.expectedErrorMessage:
						t.Errorf("expected error to be `%s`, but was `%s`", test.expectedErrorMessage, err.Error())
					default:
						done <- struct{}{}
					}
				} else {
					t.Errorf("expected to fail, but didn't")
				}
			}(t)

			select {
			case <-done:
			case <-ctx.Done():
				t.Fatalf("expected to fail, but timed out")
			}
		})
	}
}

type testServer struct {
	running bool
	reqFn   requester.Fn
	m       sync.Mutex
}

func (srv *testServer) ServeRequester(ctx context.Context, fn requester.Fn) error {
	srv.setRunning(true)
	srv.setReqFn(fn)

	<-ctx.Done()

	srv.setRunning(false)

	return nil
}

func (srv *testServer) getRunning() bool {
	srv.m.Lock()
	running := srv.running
	srv.m.Unlock()

	return running
}

func (srv *testServer) setRunning(running bool) {
	srv.m.Lock()
	srv.running = running
	srv.m.Unlock()
}

func (srv *testServer) getReqFn() requester.Fn {
	srv.m.Lock()
	reqFn := srv.reqFn
	srv.m.Unlock()

	return reqFn
}

func (srv *testServer) setReqFn(reqFn requester.Fn) {
	srv.m.Lock()
	srv.reqFn = reqFn
	srv.m.Unlock()
}

type unstartableServer struct{}

func (*unstartableServer) ServeRequester(_ context.Context, _ requester.Fn) error {
	return fmt.Errorf("unstartable server")
}
