// Package server defines the Server interface that a requester server must follow.
package server

import (
	"context"

	"gitlab.com/ravnmsg/ravn-requester/pkg/requester"
)

// Server is the interface that defines a requester server.
type Server interface {
	// ServeRequester starts the server and listens for incoming requests. The provided function fn
	// will be executed for each such requests.
	//
	// It will block until the server is closed. When the provided context terminates, the server
	// will gracefully shutdown.
	ServeRequester(ctx context.Context, fn requester.Fn) error
}
