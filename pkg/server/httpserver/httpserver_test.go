package httpserver_test

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"testing"
	"time"

	"gitlab.com/ravnmsg/ravn-requester/pkg/client/httpclient"
	"gitlab.com/ravnmsg/ravn-requester/pkg/requester"
	. "gitlab.com/ravnmsg/ravn-requester/pkg/server/httpserver"
)

func TestHTTPServer(t *testing.T) {
	srv, err := New("0")
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	go func() {
		fn := func(input *requester.Values) (*requester.Values, error) {
			v := input.GetString("inputKey")

			output := requester.Values{
				"outputKey": v,
			}

			return &output, nil
		}

		err := srv.ServeRequester(ctx, fn)
		if err != nil {
			t.Errorf("unexpected error: %s", err)
		}
	}()

	// Waits a little bit for the server to start.
	time.Sleep(4 * time.Millisecond)

	// Tests invalid endpoint.
	resp, err := http.Post(requesterURL(srv, "invalid"), "application/json", nil)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	} else if resp.StatusCode != http.StatusNotFound {
		t.Fatalf("expected request to non-step endpoint to respond with 404 Not Found")
	}
	defer resp.Body.Close()

	// Tests non-POST request.
	resp, err = http.Get(requesterURL(srv, "request"))
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	} else if resp.StatusCode != http.StatusMethodNotAllowed {
		t.Fatalf("expected non-POST request to respond with 405 Method Not Allowed")
	}
	defer resp.Body.Close()

	// Test simple request.
	clt := httpclient.New(srv.Addr())
	output, err := clt.Request(&requester.Values{"inputKey": "value"})
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	} else if output.GetString("outputKey") != "value" {
		t.Fatalf("expected output to contain `outputKey=value`, but was `%v`", output)
	}
}

func requesterURL(srv *HTTPServer, paths ...string) string {
	path := strings.Join(paths, "/")
	return fmt.Sprintf("http://%s/%s", srv.Addr(), path)
}
