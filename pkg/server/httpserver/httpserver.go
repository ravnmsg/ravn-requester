// Package httpserver defines the HTTPServer type that listens to requests over HTTP.
package httpserver

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"time"

	"gitlab.com/ravnmsg/ravn-requester/pkg/requester"
)

// HTTPServer defines an HTTP server that will offer an endpoint to execute a requester.
type HTTPServer struct {
	listener net.Listener
	server   *http.Server
}

// New creates a new instance of an HTTP server.
func New(port string) (*HTTPServer, error) {
	l, err := net.Listen("tcp", fmt.Sprintf(":%s", port))
	if err != nil {
		return nil, fmt.Errorf("error listening to port `%s`: %w", port, err)
	}

	s := http.Server{
		ReadTimeout: 2 * time.Minute,
		Handler:     http.NewServeMux(),
	}

	srv := HTTPServer{
		listener: l,
		server:   &s,
	}

	return &srv, nil
}

// Addr returns the server's listening address.
func (srv *HTTPServer) Addr() string {
	return srv.listener.Addr().String()
}

// ServeRequester starts the HTTP server and listens on the configured port for incoming requests.
// The provided function fn will be executed for each such requests.
func (srv *HTTPServer) ServeRequester(ctx context.Context, fn requester.Fn) error {
	srv.registerRootEndpoint()
	srv.registerRequesterFn(fn)

	// Shutdowns the server when the application context ends.
	go func() {
		<-ctx.Done()
		srv.stop()
	}()

	log.Printf("HTTP Server listening on `%s`...", srv.Addr())

	if err := srv.server.Serve(srv.listener); err != http.ErrServerClosed {
		return fmt.Errorf("error serving: %w", err)
	}

	return nil
}

// registerRootEndpoint registers the root endpoint that will catch all invalid paths.
func (srv *HTTPServer) registerRootEndpoint() {
	srv.mux().HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		srv.writeError(w, fmt.Errorf("invalid path `%s`", r.URL.Path), http.StatusNotFound)
	})
}

// registerRequesterFn registers the endpoint that handles the requester. When a POST request is
// received on this endpoint, the request body is converted to a map of values, the specified
// requester function is executed, and its output is returned as the response body.
func (srv *HTTPServer) registerRequesterFn(fn requester.Fn) {
	stepFunc := func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			srv.writeError(w, fmt.Errorf("only POST requests are allowed"), http.StatusMethodNotAllowed)
			return
		}

		requestBody, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			srv.writeError(
				w,
				fmt.Errorf("unexpected error while reading request body: %s", err),
				http.StatusInternalServerError,
			)
			return
		}

		responseBody, err := srv.handleRequest(fn, requestBody)
		if err != nil {
			srv.writeError(w, fmt.Errorf("error stepping: %w", err), http.StatusBadRequest)
			return
		}

		w.WriteHeader(http.StatusCreated)

		if _, err := fmt.Fprintln(w, string(responseBody)); err != nil {
			srv.writeError(
				w,
				fmt.Errorf("unexpected error while writing response body: %s", err),
				http.StatusInternalServerError,
			)
			return
		}
	}

	srv.mux().HandleFunc("/request", stepFunc)
}

func (srv *HTTPServer) handleRequest(fn requester.Fn, requestBody []byte) ([]byte, error) {
	var input *requester.Values
	if len(requestBody) > 0 {
		if err := json.Unmarshal(requestBody, &input); err != nil {
			return nil, fmt.Errorf("error parsing request body: %s", err)
		}
	}

	if input == nil {
		input = &requester.Values{}
	}

	output, err := fn(input)
	if err != nil {
		return nil, err
	}

	responseBody, err := json.Marshal(output)
	if err != nil {
		return nil, fmt.Errorf("error parsing response body: %s", err)
	}

	return responseBody, nil
}

func (srv *HTTPServer) stop() {
	// Server has one minute to finish all current connections.
	timedCtx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	log.Printf("Server shutting down...")

	if err := srv.server.Shutdown(timedCtx); err != nil {
		log.Printf("Error with server shutdown: %s", err)
	}
}

func (srv *HTTPServer) mux() *http.ServeMux {
	mux, ok := srv.server.Handler.(*http.ServeMux)
	if !ok {
		panic("server mux was not of type *http.ServeMux")
	}

	return mux
}

func (srv *HTTPServer) writeError(w http.ResponseWriter, err error, code int) {
	log.Print(err.Error())
	http.Error(w, err.Error(), code)
}
