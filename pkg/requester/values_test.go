package requester_test

import (
	"testing"

	"github.com/google/go-cmp/cmp"

	. "gitlab.com/ravnmsg/ravn-requester/pkg/requester"
)

func TestGet(t *testing.T) {
	tests := map[string]struct {
		values        *Values
		key           string
		expectedValue interface{}
	}{
		"keyPresent": {
			values:        &Values{"key": 1},
			key:           "key",
			expectedValue: 1,
		},
		"keyMissing": {
			values:        &Values{"key": 1},
			key:           "other_key",
			expectedValue: nil,
		},
	}

	for name, test := range tests {
		test := test

		t.Run(name, func(t *testing.T) {
			v := test.values.Get(test.key)
			if v != test.expectedValue {
				t.Fatalf(
					"expected value to be `%v`, but was `%v`",
					test.expectedValue,
					v,
				)
			}
		})
	}
}

func TestGetString(t *testing.T) {
	tests := map[string]struct {
		values        *Values
		key           string
		expectedValue string
	}{
		"keyPresent": {
			values:        &Values{"key": "value"},
			key:           "key",
			expectedValue: "value",
		},
		"keyMissing": {
			values:        &Values{"key": "value"},
			key:           "other_key",
			expectedValue: "",
		},
		"valueNotAString": {
			values:        &Values{"key": 1},
			key:           "key",
			expectedValue: "",
		},
	}

	for name, test := range tests {
		test := test

		t.Run(name, func(t *testing.T) {
			v := test.values.GetString(test.key)
			if v != test.expectedValue {
				t.Fatalf(
					"expected value to be `%s`, but was `%s`",
					test.expectedValue,
					v,
				)
			}
		})
	}
}

func TestSet(t *testing.T) {
	tests := map[string]struct {
		values         *Values
		key            string
		value          interface{}
		expectedValues *Values
	}{
		"NonexistingKey": {
			values:         &Values{"other_key": "other_value"},
			key:            "key",
			value:          "value",
			expectedValues: &Values{"other_key": "other_value", "key": "value"},
		},
		"ExistingKey": {
			values:         &Values{"key": "other_value"},
			key:            "key",
			value:          "value",
			expectedValues: &Values{"key": "value"},
		},
	}

	for name, test := range tests {
		test := test

		t.Run(name, func(t *testing.T) {
			test.values.Set(test.key, test.value)
			if !cmp.Equal(test.expectedValues, test.values) {
				t.Fatalf(
					"expected values to be different:\n%s",
					cmp.Diff(test.expectedValues, test.values),
				)
			}
		})
	}
}
