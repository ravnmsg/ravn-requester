// Package requester defines everything that is common to all requesters, such as the Fn function.
// It also defines the Values type that is used by the clients and servers.
package requester

// Fn defines the function that executes a request. It receives the *Values input, and returns the
// *Values output.
type Fn func(input *Values) (output *Values, err error)
