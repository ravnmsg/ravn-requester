package requester

// Values defines a map of values.
type Values map[string]interface{}

// NewValues creates an empty set of values.
func NewValues() *Values {
	return &Values{}
}

// NewValuesFromMap creates a set of values based on the provided map.
func NewValuesFromMap(sourceMap map[string]interface{}) *Values {
	values := NewValues()

	for k, v := range sourceMap {
		values.Set(k, v)
	}

	return values
}

// Get retrieves the specified key from the values.
func (v Values) Get(key string) interface{} {
	return v[key]
}

// GetString retrieves the specified key from the values and forces the result to be a string.
func (v Values) GetString(key string) string {
	value, ok := v[key]
	if !ok {
		return ""
	}

	str, ok := value.(string)
	if !ok {
		return ""
	}

	return str
}

// Set assigns the value to the specified key.
func (v Values) Set(key string, value interface{}) {
	v[key] = value
}
