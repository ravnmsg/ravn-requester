// Package client defines the Client interface that a requester client must follow.
package client

import (
	"gitlab.com/ravnmsg/ravn-requester/pkg/requester"
)

// Client is the interface that defines a requester client.
type Client interface {
	// Request performs a request to the requester server.
	Request(input *requester.Values) (output *requester.Values, err error)
}
