package httpclient_test

import (
	"context"
	"testing"
	"time"

	. "gitlab.com/ravnmsg/ravn-requester/pkg/client/httpclient"
	"gitlab.com/ravnmsg/ravn-requester/pkg/requester"
	"gitlab.com/ravnmsg/ravn-requester/pkg/server/httpserver"
)

func TestHTTPClient(t *testing.T) {
	srv, err := httpserver.New("0")
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	go func() {
		fn := func(input *requester.Values) (*requester.Values, error) {
			v := input.GetString("inputKey")

			output := requester.Values{
				"outputKey": v,
			}

			return &output, nil
		}

		err := srv.ServeRequester(ctx, fn)
		if err != nil {
			t.Errorf("unexpected error: %s", err)
		}
	}()

	// Waits a little bit for the server to start.
	time.Sleep(4 * time.Millisecond)

	clt := New(srv.Addr())
	output, err := clt.Request(&requester.Values{"inputKey": "value"})
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	} else if output.GetString("outputKey") != "value" {
		t.Fatalf("expected output to contain `outputKey=value`, but was `%v`", output)
	}
}
