// Package httpclient defines the HTTPClient type that can connect to a requester server over HTTP.
package httpclient

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/ravnmsg/ravn-requester/pkg/requester"
)

// HTTPClient defines an HTTP client that will perform the necessary request to execute a requester.
type HTTPClient struct {
	requesterHost string
}

// New creates a new instance of an HTTP client to make requests to a requester server.
func New(requesterHost string) *HTTPClient {
	clt := HTTPClient{
		requesterHost: requesterHost,
	}

	return &clt
}

// Request performs an HTTP request to the requester at the configured host.
func (clt *HTTPClient) Request(input *requester.Values) (output *requester.Values, err error) {
	url := fmt.Sprintf("http://%s/request", clt.requesterHost)

	var requestBody []byte
	if input != nil {
		requestBody, err = json.Marshal(input)
		if err != nil {
			return nil, fmt.Errorf("error marshaling request body: %s", err)
		}
	}

	//nolint:gosec // The URI is configured when the client is created.
	resp, err := http.Post(url, "application/json", bytes.NewReader(requestBody))
	if err != nil {
		return nil, fmt.Errorf("error requesting requester: %s", err)
	}
	defer resp.Body.Close()

	responseBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("error reading response body: %s", err)
	}

	if err := json.Unmarshal(responseBody, &output); err != nil {
		return nil, fmt.Errorf("error unmarshaling response body: %s", err)
	}

	return
}
